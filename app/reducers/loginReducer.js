import { MODIFICA_EMAIL, MODIFICA_SENHA, SUCESSO_LOGIN, SUCESSO_VALIDATION, ERRO_VALIDATION } from '../actions/types';

const INITIAL_STATE = {
    email: '',
    senha: '',
    visible: true
}

export default (state = INITIAL_STATE, action) => {
    if (action.type == MODIFICA_EMAIL) {
        return { ...state, email: action.payload }
    }
    if (action.type == MODIFICA_SENHA) {
        return { ...state, senha: action.payload }
    }
    if (action.type == SUCESSO_LOGIN) {
        return { ...state, email: '', senha: '' }
    }
    if (action.type == SUCESSO_VALIDATION) {
        return { ...state, visible: !state.visible }
    }
    if (action.type == ERRO_VALIDATION) {
        return { ...state, visible: !state.visible }
    }
    return state;
}