import { combineReducers } from 'redux';
import LoginReducer from './loginReducer';
import ListaOvosReducer from './listaOvosReducer';
import DetalhesOvoReducer from './detalhesOvoReducer';

export default combineReducers ({
    LoginReducer: LoginReducer,
    ListaOvosReducer: ListaOvosReducer,
    DetalhesOvoReducer: DetalhesOvoReducer
});