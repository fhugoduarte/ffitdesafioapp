import { MODIFICA_OVOS, SUCESSO_MODIFICA_OVOS } from '../actions/types';

const INITIAL_STATE = {
    ovos: ''
}

export default (state = INITIAL_STATE, action) => {
    if (action.type == MODIFICA_OVOS) {
        return { ...state, ovos: action.payload }
    }
    if (action.type == SUCESSO_MODIFICA_OVOS) {
        return { ...state, ovos: action.payload }
    }
    return state;
}