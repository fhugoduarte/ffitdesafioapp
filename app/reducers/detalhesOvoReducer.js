import { SUCESSO_BUSCAR_OVO } from '../actions/types';

const INITIAL_STATE = {
    ovo: ''
}

export default (state = INITIAL_STATE, action) => {
    if (action.type == SUCESSO_BUSCAR_OVO) {
        return { ...state, ovo: action.payload }
    }
    return state;
}