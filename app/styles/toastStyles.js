import { StyleSheet } from "react-native";

const ToastStyles = StyleSheet.create({
    toastError: {
      backgroundColor: "#FF0000",
      width: 300,
      borderRadius: 15,
      shadowColor: "#000000",
      shadowOpacity: 0.5,
      opacity: 0.8,
      marginTop: 30
    },
    toastSuccess: {
        backgroundColor: "#008000",
        width: 300,
        borderRadius: 15,
        shadowColor: "#000000",
        shadowOpacity: 0.5,
        opacity: 0.8,
        marginTop: 30
    }
});

export default ToastStyles;