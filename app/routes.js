import React, { Component } from 'react';
import { AppRegistry } from "react-native";
import { Router, Scene, Stack, Drawer, Reducer } from 'react-native-router-flux';

import FormLogin from './components/login/loginForm';
import ListaOvos from './components/ovos/listaOvos';
import DetalhesOvo from './components/ovos/detalhesOvo';

import Menu from './components/drawer/menu';

export default class Routes extends Component {
    render(){
        return(
            <Router>
                <Stack key="root">
                    <Scene key='formLogin' component={ FormLogin } 
                        title='Login' hideNavBar={ true } initial/>
                    <Drawer
                        contentComponent={ Menu }
                        key="menu"
                        hideNavBar
                    >
                        <Scene key='listaOvos' component={ ListaOvos } 
                            title='Lista de Ovos' panHandlers={null} />
                        <Scene key='detalhesOvo' component={ DetalhesOvo } 
                            title='Detalhes' panHandlers={null} />
                    </Drawer>
                </Stack>
            </Router>
        );
    }
}

AppRegistry.registerComponent("Routes", Routes);