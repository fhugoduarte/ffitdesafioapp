import { AsyncStorage } from "react-native";
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import Toast from 'react-native-root-toast2';
import ToastStyles from '../styles/toastStyles';

import { API_URL, STORAGE_EGGS } from '../utils/constants';

import{ SUCESSO_BUSCAR_OVO } from './types';

export const buscarOvo = (id) => {
    return dispatch => {
        axios({
            method: 'get',
            url: API_URL + '/easter-eggs/' + id,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            sucessoBuscarOvo(response, dispatch);
        }).catch((erro) => {
            erroBuscarOvo(erro, dispatch);
        });
    }
}

const sucessoBuscarOvo = (response, dispatch) => {
    let ovo = JSON.stringify(response.data);

    dispatch({ type: SUCESSO_BUSCAR_OVO, payload: ovo })
}

const erroBuscarOvo = (erro, dispatch) => {
    Toast.show(erro.response.data.error, { 
        duration: Toast.durations.SHORT,
        position: Toast.positions.TOP,
        containerStyle: ToastStyles.toastError
    });
}