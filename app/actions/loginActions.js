import { AsyncStorage } from "react-native";
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import Toast from 'react-native-root-toast2';
import ToastStyles from '../styles/toastStyles';

import { API_URL, STORAGE_USER } from '../utils/constants';

import{
    MODIFICA_EMAIL,
    MODIFICA_SENHA,
    SUCESSO_LOGIN,
    ERRO_LOGIN,
    SUCESSO_VALIDATION,
    ERRO_VALIDATION
} from './types';

export const modificaEmail = (texto) => {
    return {
        type: MODIFICA_EMAIL,
        payload: texto
    }
}

export const modificaSenha = (texto) => {
    return {
      type: MODIFICA_SENHA,
      payload: texto
    }
}

export const login = ({ email, senha }) => {    
    return dispatch => {
        axios({
            method: 'post',
            url: API_URL + '/auth',
            headers: {
                'Content-Type': 'application/json'
            },
            data:{
                email: email,
                senha: senha
            }
        }).then((response) => {
            sucessoLogin(response, dispatch);
        }).catch((erro) => {
            erroLogin(erro, dispatch);
        });
    }
}

const sucessoLogin = (response, dispatch) => {
    Toast.show('Login realizado com Sucesso!', { 
        duration: Toast.durations.SHORT, 
        position: Toast.positions.TOP,
        containerStyle: ToastStyles.toastSuccess
    });
    AsyncStorage.setItem(STORAGE_USER, JSON.stringify(response.data));
    dispatch ({ type: SUCESSO_LOGIN });
    Actions.listaOvos();
}

const erroLogin = (erro, dispatch) => {
    Toast.show(erro.response.data.error, { 
        duration: Toast.durations.SHORT,
        position: Toast.positions.TOP,
        containerStyle: ToastStyles.toastError
    });
    dispatch({ type: ERRO_LOGIN })
}

export const validaUsuarioLogado = (user) => {
    return dispatch => {

        if(user == null){
            usuarioInvalido(dispatch);
        }else{
            usuarioValido(dispatch);
        }
    }
}

const usuarioValido = (dispatch) => {
    dispatch ({ type: SUCESSO_VALIDATION })
    Actions.listaOvos();
}

const usuarioInvalido = (dispatch) => {
    dispatch ({ type: ERRO_VALIDATION })
}