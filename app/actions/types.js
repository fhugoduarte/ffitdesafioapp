
export const MODIFICA_EMAIL = 'modifica_email';
export const MODIFICA_SENHA = 'modifica_senha';

export const SUCESSO_VALIDATION = 'sucesso_validation';
export const ERRO_VALIDATION = 'erro_validation';

export const MODIFICA_OVOS = 'modifica_ovos';
export const SUCESSO_MODIFICA_OVOS = 'sucesso_modifica_ovos';
export const SUCESSO_BUSCAR_OVO = 'sucesso_buscar_ovo';

export const SUCESSO_LOGIN = 'sucesso_login';
export const ERRO_LOGIN = 'erro_login';

export const SUCESSO_LOGOUT = 'sucesso_logout';
export const ERRO_LOGOUT = 'erro_logout';