import { AsyncStorage } from "react-native";
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import Toast from 'react-native-root-toast2';
import ToastStyles from '../styles/toastStyles';

import { API_URL, STORAGE_EGGS } from '../utils/constants';

import{ MODIFICA_OVOS, SUCESSO_MODIFICA_OVOS } from './types';

export const modificaOvos = (texto) => {
    return {
      type: MODIFICA_OVOS,
      payload: texto
    }
}

export const pegaOvos = () => {
    return dispatch => {
        axios({
            method: 'get',
            url: API_URL + '/easter-eggs',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            sucessoPegaOvos(response, dispatch);
        }).catch((erro) => {
            erroPegaOvos(erro, dispatch);
        });
    }
}

const sucessoPegaOvos = (response, dispatch) => {
    let eggs = JSON.stringify(response.data);

    AsyncStorage.setItem(STORAGE_EGGS, eggs);
    dispatch({ type: SUCESSO_MODIFICA_OVOS, payload: eggs });
}

const erroPegaOvos = (erro, dispatch) => {
    Toast.show(erro.response.data.error, { 
        duration: Toast.durations.SHORT,
        position: Toast.positions.TOP,
        containerStyle: ToastStyles.toastError
    });
}

export const buscarOvo = (id) => {
    return dispatch => {
        Actions.detalhesOvo({ id : id });
    }
}

const sucessoBuscarOvo = (response, dispatch) => {
    let ovo = JSON.stringify(response.data);
    Actions.detalhesOvo(ovo);
}

const erroBuscarOvo = (erro, dispatch) => {
    Toast.show(erro.response.data.error, { 
        duration: Toast.durations.SHORT,
        position: Toast.positions.TOP,
        containerStyle: ToastStyles.toastError
    });
}