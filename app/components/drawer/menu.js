import React, { Component } from "react";
import {
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  StyleSheet,
  AsyncStorage
} from "react-native";

import axios from 'axios';

import { Actions } from 'react-native-router-flux';
import { Thumbnail, Text } from 'native-base';

import { Button, Icon } from 'native-base';
import { API_URL, STORAGE_USER, STORAGE_EGGS } from '../../utils/constants';

import Toast from 'react-native-root-toast2';
import ToastStyles from '../../styles/toastStyles';

export default class Menu extends Component {

    componentWillMount(){
        AsyncStorage.getItem(STORAGE_USER)
          .then(user => {
              let userJSON = JSON.parse(user);
              this.setState({foto: userJSON.foto});
              this.setState({nome: userJSON.nome});
              this.setState({email: userJSON.email});
              this.setState({senha: userJSON.senha});
          } )   
          .catch();
    }

    constructor(props){
        super(props);
        this.state = {
            foto: '',
            nome: '',
            email: '',
            senha: ''
        };
    }

    _logout() {

        axios({
            method: 'delete',
            url: API_URL + '/auth',
            headers: {
                'Content-Type': 'application/json'
            },
            data:{
                email: this.state.email,
                senha: this.state.senha
            }
        }).then((response) => {
            AsyncStorage.multiRemove([STORAGE_USER, STORAGE_EGGS])
                .then(Actions.popTo('formLogin'))
                .catch();
        }).catch((erro) => {
            Toast.show(erro.response.data.error, { 
                duration: Toast.durations.SHORT,
                position: Toast.positions.TOP,
                containerStyle: ToastStyles.toastError
            });
        });

        
    }

    _listarOvos() {
        Actions.listaOvos();
    }

    render() {
        if(this.state.foto != '' && this.state.nome != '' && this.state.email != ''){
            return (
                <View> 
                    <View style={styles.header}>
                        <Thumbnail circular source={{url: this.state.foto}} style={{marginTop: 30}} />
                        <Text style={{marginTop:10}}>{this.state.nome}</Text>
                        <Text note >{this.state.email}</Text>
                    </View > 
                    <View style={styles.menu}>
                        <Button iconLeft light style={styles.menuButton} onPress={() => this._listarOvos()}>
                            <Icon name='search' />
                            <Text>Ver Ovos</Text>
                        </Button>
                        <Button iconLeft light style={styles.menuButton} onPress={() => this._logout()}>
                            <Icon name='exit' />
                            <Text style={{fontSize: 18}}>Logout</Text>
                        </Button>
                    </View> 
                </View>
                );
        }else{
            return( 
                <View>
                </View>
            );
        }
        
      }
}

const styles = StyleSheet.create({
    header: {
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 30
    },
    menu:{
        marginTop: 40
    },
    image: {
        height: 100,
        width: 100,
        marginTop: 30,
        borderRadius: 50,
        alignSelf:'center'
    },
    menuButton:{
        marginTop:5,
        width: '100%',
        height:40,
        justifyContent: 'flex-start',
      alignItems: 'flex-start'
    }
 });