import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    cardHeader: {
        backgroundColor: '#F5F5F5'
    },
    icon: {
        fontSize: 20,
        color: '#006400'
    },
    text: {
        color: '#006400',
        fontSize:18
    },
    cardBody:{
        alignItems: 'center', 
        justifyContent: 'center'
    },
    image: {
        height: 150,
        width: 150
    }
    
});

export default styles;