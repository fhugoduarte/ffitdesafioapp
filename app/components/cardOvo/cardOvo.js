import React, { Component }  from 'react';
import { Card, CardItem, Left, Thumbnail, Text, Icon, Button, Body, Right } from 'native-base';
import { Image, TouchableOpacity, ImageBackground } from 'react-native';

import styles from './cardOvoStyle';

export default class CardOvo extends Component{

    render(){
        return (
            <Card>
                <CardItem style={styles.cardHeader}>
                    <Left>
                        <Thumbnail source={require('../../imgs/logo.png')} />
                        <Body>
                            <Text>{ this.props.ovo.nome }</Text>
                            <Text note> { this.props.ovo.marca } </Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem cardBody style={styles.cardBody}>
                    <Image source={{uri: this.props.ovo.imagem }} style={styles.image} />
                </CardItem>
                <CardItem >
                    <Left> 
                        <Icon active name='logo-usd' style={styles.icon}/>
                        <Text style={styles.text}>
                            {this.props.ovo.preco} Reais
                        </Text>
                    </Left>
                </CardItem>
            </Card>
        );
    }
}