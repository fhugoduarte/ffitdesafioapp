import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    logo:{
        marginTop: 40,
        marginBottom: 40,
        width: 150,
        height: 150,
        alignSelf: 'center'
    },
    titulo: {
        fontSize:25,
        color: "#8B4513",
        marginLeft: 40,
        marginTop: 15,
        fontWeight: 'bold'
    },
    input:{
        marginLeft: 40,
        marginRight: 40,
        marginTop: 20,
        paddingHorizontal: 30,
        backgroundColor: '#DCDCDC',
        height: 50,
        borderWidth: 1,
        borderRadius: 10,
        fontSize: 18,
        borderColor: '#696969'
    },
    loginButton: {
        alignItems: 'center',
        backgroundColor: '#8B4513',
        padding: 15,
        marginTop: 20,
        marginLeft: 40,
        marginRight: 40,
        borderWidth: 1,
        borderRadius: 10,
        marginBottom: 50
    },
    textButton:{
        textAlign: "center",
        fontWeight: 'bold',
        color: '#ffffff',
        fontSize: 18
    }
});

export default styles;