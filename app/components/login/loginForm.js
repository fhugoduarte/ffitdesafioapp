import React, { Component }  from 'react';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast2';
import ToastStyles from '../../styles/toastStyles';
import Spinner from 'react-native-loading-spinner-overlay';
import {
    StyleSheet,
    TextInput,
    View,
    TouchableOpacity,
    AsyncStorage,
    Image,
    Text,
    ImageBackground
} from "react-native";

import { modificaEmail, modificaSenha, login, validaUsuarioLogado } from '../../actions/loginActions';
import { STORAGE_USER } from '../../utils/constants';
import styles from './loginFormStyle';

class LoginForm extends Component{

    componentWillMount() {
        AsyncStorage.getItem(STORAGE_USER)
          .then(user => {
                this.props.validaUsuarioLogado(user)
            })   
          .catch( () => this.props.validaUsuarioLogado(null));
    }

    _login(){
        const email = this.props.email;
        const senha = this.props.senha;
        if(email == '' || senha == ''){
            Toast.show("Os campos 'Email' e 'Senha' são obrigatórios!", { 
                duration: Toast.durations.SHORT,
                position: Toast.positions.TOP,
                containerStyle: ToastStyles.toastError
            });
        }else{
            this.props.login({ email, senha});
        }
    }

    render(){
        return(
            <ImageBackground style={{ flex: 1, width: null }} 
                        imageStyle={{resizeMode: 'stretch'}} 
                        source={require('../../imgs/background.jpeg')}>
                <View>
                    <Image source={require('../../imgs/logo.png')} style={ styles.logo }  />

                    <Spinner visible={this.props.visible} 
                        textContent={"Validando suas informações..."} 
                        textStyle={{color: '#FFF'}} 
                    />
                    
                    <Text style={ styles.titulo }>Login</Text>
                    <TextInput
                        ref="email"
                        style={ styles.input } 
                        value={ this.props.email }
                        placeholder="Email"
                        placeholderTextColor= '#ffffff'
                        keyboardType="email-address"
                        returnKeyType="next"
                        autoCapitalize='none'
                        autoCorrect={false}
                        onSubmitEditing={()=>this.senhaInput.focus()}
                        onChangeText={ texto => this.props.modificaEmail(texto) }
                    />

                    <TextInput
                        ref="senha"
                        style={ styles.input } 
                        value={ this.props.senha }
                        placeholder="Senha"
                        placeholderTextColor= '#ffffff'
                        returnKeyType="go"
                        secureTextEntry
                        ref={(input)=> this.senhaInput = input}
                        onSubmitEditing={()=>this._login()}
                        onChangeText={ texto => this.props.modificaSenha(texto) }
                    />
                        
                    <TouchableOpacity style={ styles.loginButton } 
                            onPress={ () => { this._login() } }>
                        <Text style={ styles.textButton }>LOGAR</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        );
    }

}



const mapStateToProps = state => (
    {
        email: state.LoginReducer.email,
        senha: state.LoginReducer.senha,
        visible: state.LoginReducer.visible
    }
)

export default connect(mapStateToProps, { modificaEmail, modificaSenha, login, validaUsuarioLogado })(LoginForm);