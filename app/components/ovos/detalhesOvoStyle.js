import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        height: 150,
        width: 150,
        marginTop: 30
    },
    card:{
        backgroundColor: '#8B4513',
        opacity: 0.8,
        borderWidth: 2,
        borderColor: '#fff',
        borderRadius: 20,
        marginTop: 30,
    },
    title: {
        fontWeight: 'bold',
        color: '#ffffff',
        fontSize: 20,
        marginLeft: 10,
        marginRight: 20,
        marginTop: 10,
        marginBottom: 10
    },
    text: {
        color: '#ffffff',
        fontSize: 18,
        marginLeft: 10,
        marginRight: 20,
        marginTop: 5,
        marginBottom: 5
    }
    
});

export default styles;