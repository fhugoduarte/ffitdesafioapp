import React, { Component }  from 'react';
import { connect } from 'react-redux';
import {
    StyleSheet,
    TextInput,
    View,
    TouchableOpacity,
    AsyncStorage,
    Image,
    Text,
    FlatList,
    ImageBackground
} from "react-native";

import CardOvo from '../cardOvo/cardOvo';

import { modificaOvos, pegaOvos, buscarOvo } from '../../actions/listaOvosActions';
import { STORAGE_EGGS } from '../../utils/constants';

class ListaOvos extends Component{

    componentWillMount() {
        AsyncStorage.getItem(STORAGE_EGGS)
        .then(eggs => {
            if(eggs == null){
                this.props.pegaOvos()
            }else{
                this.props.modificaOvos(eggs)
            }
        })
        .catch(() => this.props.pegaOvos());
    }

    _keyExtractor = (item, index) => item.id.toString();

    _buscar(id){
        this.props.buscarOvo(id);
    }

    render(){

        if(this.props.ovos != ''){
            const eggs = JSON.parse(this.props.ovos);
            return (
                <ImageBackground style={{ flex: 1, width: null }} 
                    imageStyle={{resizeMode: 'stretch'}} 
                    source={require('../../imgs/background.jpeg')}>
                    <FlatList 
                        data={eggs}
                        renderItem={({item}) => 
                            <TouchableOpacity onPress={() => this._buscar(item.id)}>
                                <CardOvo ovo={item}/>
                            </TouchableOpacity>
                         }
                        keyExtractor={this._keyExtractor}
                    />
                </ImageBackground>
            );
        }else{
            return(
                <ImageBackground style={{ flex: 1, width: null }} 
                    imageStyle={{resizeMode: 'stretch'}} 
                    source={require('../../imgs/background.jpeg')}>'
                    <View>
                        <Text></Text>
                    </View>
                </ImageBackground>
            );
        }
        
    }
}

const mapStateToProps = state => (
    {
        ovos: state.ListaOvosReducer.ovos
    }
)

export default connect(mapStateToProps, { modificaOvos, pegaOvos, buscarOvo })(ListaOvos);