import React, { Component } from "react";
import { connect } from 'react-redux';
import {
  Text,
  View,
  ImageBackground,
  Image
} from "react-native";

import { Thumbnail } from 'native-base';

import { buscarOvo } from '../../actions/detalhesOvoActions';
import styles from './detalhesOvoStyle';

class DetalhesOvo extends Component {

    componentWillMount() {
        this.props.buscarOvo(this.props.id);
    }

    render() {

        if(this.props.ovo != ''){
            const egg = JSON.parse(this.props.ovo);
            return (
                <ImageBackground style={{ flex: 1, width: null }} 
                        imageStyle={{resizeMode: 'stretch'}} 
                        source={require('../../imgs/background.jpeg')}>
                    <View style={styles.container}>
                        <Thumbnail circular source={{uri: egg.imagem}} style={styles.image} />
                        <View style={styles.card}>
                            <Text style={styles.title}>Informações</Text>
                            <Text style={styles.text}>Nome: {egg.nome}</Text>
                            <Text style={styles.text}>Marca: {egg.marca}</Text>
                            <Text style={styles.text}>Sabor: {egg.sabor}</Text>
                            <Text style={styles.text}>Peso: {egg.peso} g</Text>
                            <Text style={styles.text}>Preço: R$ {egg.preco} </Text>
                        </View>
                    </View>
                </ImageBackground>
            );
        }else{
            return(
                <ImageBackground style={{ flex: 1, width: null }} 
                    imageStyle={{resizeMode: 'stretch'}} 
                    source={require('../../imgs/background.jpeg')}>'
                    <View>
                        <Text></Text>
                    </View>
                </ImageBackground>
            );
        }
      }
}

const mapStateToProps = state => (
    {
        ovo: state.DetalhesOvoReducer.ovo
    }
)

export default connect(mapStateToProps, { buscarOvo })(DetalhesOvo);